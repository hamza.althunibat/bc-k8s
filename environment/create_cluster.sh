#!/usr/bin/bash
sudo rm -rf $PWD/environment/tmp/* && \
k3d cluster create dev-cluster \
    --no-lb -a 3 \
    --k3s-server-arg '--disable=servicelb' --k3s-server-arg '--disable=traefik' \
    -v "$PWD/environment/tmp:/var/lib/rancher/k3s/storage@agent[0,1,2]" \
    -v "$PWD/dm/client:/dm@agent[0,1,2]" \
    -v "$PWD/am/client:/am@agent[0,1,2]" \
    -v "$PWD/nz/client:/nz@agent[0,1,2]" \
    -v "$PWD/orderer/client:/orderer@agent[0,1,2]"

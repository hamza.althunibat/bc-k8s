# Setting up production like Kubernetes environment for Hyperledger Fabric

In this repository I've put step by step guide how to deploy Hyperledger Fabric 2.3.x on production like kubernetes environment on your Ubuntu machine.

## Prerequisite

1.  Ubuntu server / Desktop 20.04 LTS or newer
2.  Docker CE -- I've used [docker.io](http://docker.io) package.
3.  k3d [https://github.com/rancher/k3d](https://github.com/rancher/k3d "k3d - Github")
4.  k9s -- optional but very useful [https://github.com/derailed/k9s](https://github.com/derailed/k9s "k9s - github")
5.  golang 1.16
6.  git, curl, vscode,jq
7.  domain name "DNS", I have purchased "secureledger.xyz" from Cloudflare for 8+ $.

## Network Structure

The blockchain network part of the setup has 3 organizations and orderer

each organization has three pears

## Steps

- clone the repository into your local path `"$HOME/ubuntu/go/src/gitlab/hamza.althunibat"` `git clone https://gitlab.com/hamza.althunibat/bc-k8s.git`
- change the directory `cd $HOME/ubuntu/go/src/gitlab/hamza.althunibat/bc-k8s.git`
- create k3d cluster using the script available under `"environment/create_cluster.sh"`

```bash
#!/usr/bin/bash
k3d cluster create dev-cluster \
    --no-lb -a 3 \
    --k3s-server-arg '--disable=servicelb' --k3s-server-arg '--disable=traefik' \
    -v "$PWD/environment/tmp:/var/lib/rancher/k3s/storage@agent[0,1,2]" \
    -v "$PWD/dm/client:/dm@agent[0,1,2]" -v "$PWD/am/client:/am@agent[0,1,2]" \
    -v "$PWD/nz/client:/nz@agent[0,1,2]" -v "$PWD/orderer/client:/orderer@agent[0,1,2]"
```

- The script will deploy k3d cluster with three agents without deploying `"servicelb" & "traefik"`, additionally it maps volumes as following:
    - `environment/tmp` will be the root folder for "local-path" the storage provisioner built into k3s / k3d --> default path on each agent `/var/lib/rancher/k3s/storage`.
    - `am/client` path used to hold all CA files for "AM" organization
    - `dm/client` path used to hold all CA files for "DM" organization
    - `nz/client` path used to hold all CA files for "NZ" organization
    - `orderer/client` path used to hold all CA files for "orderer" organization
- Check what is the ip subnet docker used for  network `"k3d-dev-cluster"` using the command `docker network inspect k3d-dev-cluster | jq -r 'map(.IPAM.Config[].Subnet)[]'`
- Update file `"environment/metallb-config.yaml"` to indicate the used ip range --> default `172.18.0.0/16````yaml
    apiVersion: v1
    kind: ConfigMap
    metadata:
      namespace: metallb-system
      name: config
    data:
      config: |
        address-pools:
        - name: default
          protocol: layer2
          addresses:
          - 172.18.6.0-172.18.15.250
    ```
- Run script `"./environment/setup_metallb.sh"````bash
    #!/usr/bin/zsh
    
    kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/namespace.yaml
    kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/metallb.yaml
    kubectl apply -f ./environment/metallb-config.yaml
    ```
- Now, we start with first organization, the `"orderer organization"`
    - change directory `cd orderer`
    - create root CA to be used by fabric-ca servers' cluster
    - ```bash
        #!/usr/bin/bash
        openssl ecparam -name prime256v1 -genkey -noout \
            -out $PWD/rootCA/private/orderer.secureledger.xyz.key.pem
        openssl req -config $PWD/rootCA/ca.conf -new -x509 -sha256 -extensions v3_ca \
            -key $PWD/rootCA/private/orderer.secureledger.xyz.key.pem \
            -out $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem -days 3650 \
            -subj "/C=AE/ST=Dubai/L=Dubai/O=Orderer Organization/OU=IT/CN=orderer.secureledger.xyz"
        kubectl create secret tls ca-root --cert=$PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem \
            --key=$PWD/rootCA/private/orderer.secureledger.xyz.key.pem \
            --dry-run=client \
            -n orderer \
            -o yaml >&$PWD/deployment/ca/ca-root-secret.yaml
        ```
#!/bin/sh

echo "Register user" &&
    fabric-ca-client register --caname ca-org-2 --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
    echo "Register the org admin" &&
    fabric-ca-client register --caname ca-org-2 --id.name dmAdmin --id.secret dmAdminpw --id.type admin --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
    echo "Generate the user msp" &&
    mkdir -p $PWD/client/users/User1@org-2.svc &&
    fabric-ca-client enroll -u https://user1:user1pw@ca.org-2.secureledger.xyz:443 --caname ca-org-2 -M $PWD/client/users/User1@org-2.svc/msp --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/users/User1@org-2.svc/msp/config.yaml &&
    echo "Generate the org admin msp" &&
    mkdir -p $PWD/client/users/Admin@org-2.svc &&
    fabric-ca-client enroll -u https://dmAdmin:dmAdminpw@ca.org-2.secureledger.xyz:443 --caname ca-org-2 -M $PWD/client/users/Admin@org-2.svc/msp --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/users/Admin@org-2.svc/msp/config.yaml &&
    echo "done!"

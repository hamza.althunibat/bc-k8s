#!/bin/sh

echo "Enroll the CA admin" &&
    sleep 1 &&
    fabric-ca-client enroll -u https://admin:adminpw@ca.org-2.secureledger.xyz:443 --caname ca-org-2 --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
    echo "done!"

#!/bin/sh

PEER=$1
if [ -z "$1" ]; then
  PEER=peer-1
fi

echo "register peer" &&
  fabric-ca-client register --caname ca-org-2 --id.name $PEER --id.secret "${PEER}pw" --id.type peer --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
  echo "Generate the peer msp" &&
  fabric-ca-client enroll -u "https://${PEER}:${PEER}pw@ca.org-2.secureledger.xyz:443" --caname ca-org-2 -M $PWD/client/peers/${PEER}.org-2.svc/msp --csr.hosts "${PEER}.org-2.svc" --csr.hosts ${PEER}.org-2.secureledger.xyz --csr.hosts localhost --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
  echo "copying msp config" &&
  cp $PWD/tmp/msp-config.yaml $PWD/client/peers/${PEER}.org-2.svc/msp/config.yaml &&
  echo "Generate the ${PEER} tls certificates" &&
  fabric-ca-client enroll -u "https://${PEER}:${PEER}pw@ca.org-2.secureledger.xyz:443" --caname ca-org-2 -M $PWD/client/peers/${PEER}.org-2.svc/tls --enrollment.profile tls --csr.hosts ${PEER}.org-2.svc --csr.hosts ${PEER}.org-2.secureledger.xyz --csr.hosts localhost --tls.certfiles $PWD/rootCA/certs/org-2.secureledger.xyz.crt.pem &&
  cp $PWD/client/peers/${PEER}.org-2.svc/tls/tlscacerts/* $PWD/client/peers/${PEER}.org-2.svc/tls/ca.crt &&
  cp $PWD/client/peers/${PEER}.org-2.svc/tls/signcerts/* $PWD/client/peers/${PEER}.org-2.svc/tls/server.crt &&
  cp $PWD/client/peers/${PEER}.org-2.svc/tls/keystore/* $PWD/client/peers/${PEER}.org-2.svc/tls/server.key &&
  mkdir -p $PWD/client/peers/${PEER}.org-2.svc/msp/tlscacerts &&
  cp $PWD/client/peers/${PEER}.org-2.svc/tls/tlscacerts/* $PWD/client/peers/${PEER}.org-2.svc/msp/tlscacerts/tlsca.org-2.secureledger.xyz.pem &&
  echo "done"

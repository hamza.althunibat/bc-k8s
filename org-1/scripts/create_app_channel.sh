#!/bin/sh
export PATH="$PATH:$PWD/../bin"
export FABRIC_CA_SERVER_CA_NAME=ca-org-1
export FABRIC_CA_CLIENT_URL=https://ca.org-1.secureledger.xyz
export FABRIC_CA_CLIENT_HOME=$PWD/client
export ORDERER_CA=$PWD/../orderer/rootCA/certs/orderer.secureledger.xyz.crt.pem
export CORE_PEER_ID=cli.org-1.secureledger.xyz
export FABRIC_LOGGING_SPEC=DEBUG
export CORE_PEER_ADDRESS=peer-1.org-1.secureledger.xyz:7051
export CORE_PEER_LOCALMSPID=DMMSP
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_CERT_FILE=$PWD/client/peers/peer-1.org-1.svc/tls/server.crt
export CORE_PEER_TLS_KEY_FILE=$PWD/client/peers/peer-1.org-1.svc/tls/server.key
export CORE_PEER_TLS_ROOTCERT_FILE=$PWD/client/msp/tlscacerts/org-1.secureledger.xyz.crt.pem
export CORE_PEER_MSPCONFIGPATH=$PWD/client/users/Admin@org-1.svc/msp
export FABRIC_CA_CLIENT_HOME=$PWD/client
#export FABRIC_CFG_PATH=$PWD/configtx
peer channel create -o server-1.orderer.secureledger.xyz:7050 -c abcchannel -f $PWD/client/channel-artifacts/abcchannel.tx --outputBlock $PWD/client/channel-artifacts/abcchannel.block --tls --cafile $ORDERER_CA
peer channel create -o server-1.orderer.secureledger.xyz:7050 -c xyzchannel -f $PWD/client/channel-artifacts/xyzchannel.tx --outputBlock $PWD/client/channel-artifacts/xyzchannel.block --tls --cafile $ORDERER_CA

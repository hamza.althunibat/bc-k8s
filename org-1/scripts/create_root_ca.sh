#!/usr/bin/bash
openssl ecparam -name prime256v1 -genkey -noout \
    -out $PWD/rootCA/private/org-1.secureledger.xyz.key.pem &&
    openssl req -config $PWD/rootCA/ca.conf -new -x509 -sha256 -extensions v3_ca \
        -key $PWD/rootCA/private/org-1.secureledger.xyz.key.pem \
        -out $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem -days 3650 \
        -subj "/C=AE/ST=Dubai/L=Dubai/O=Organization One/OU=IT/CN=org-1.secureledger.xyz" &&
    kubectl create secret tls ca-root --cert=$PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem \
        --key=$PWD/rootCA/private/org-1.secureledger.xyz.key.pem \
        --dry-run=client \
        -n org-1 \
        -o yaml >&$PWD/deployment/ca/ca-root-secret.yaml

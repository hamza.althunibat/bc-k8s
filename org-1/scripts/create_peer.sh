#!/bin/sh

PEER=$1
if [ -z "$1" ]; then
  PEER=peer-0
fi

echo "register peer" &&
  fabric-ca-client register --caname ca-org-1 --id.name $PEER --id.secret "${PEER}pw" --id.type peer --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
  echo "Generate the peer msp" &&
  fabric-ca-client enroll -u "https://${PEER}:${PEER}pw@ca.org-1.secureledger.xyz:443" --caname ca-org-1 -M $PWD/client/peers/${PEER}.org-1.svc/msp --csr.hosts "${PEER}.org-1.svc" --csr.hosts ${PEER}.org-1.secureledger.xyz --csr.hosts localhost --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
  echo "copying msp config" &&
  cp $PWD/tmp/msp-config.yaml $PWD/client/peers/${PEER}.org-1.svc/msp/config.yaml &&
  echo "Generate the ${PEER} tls certificates" &&
  fabric-ca-client enroll -u "https://${PEER}:${PEER}pw@ca.org-1.secureledger.xyz:443" --caname ca-org-1 -M $PWD/client/peers/${PEER}.org-1.svc/tls --enrollment.profile tls --csr.hosts ${PEER}.org-1.svc --csr.hosts ${PEER}.org-1.secureledger.xyz --csr.hosts localhost --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
  cp $PWD/client/peers/${PEER}.org-1.svc/tls/tlscacerts/* $PWD/client/peers/${PEER}.org-1.svc/tls/ca.crt &&
  cp $PWD/client/peers/${PEER}.org-1.svc/tls/signcerts/* $PWD/client/peers/${PEER}.org-1.svc/tls/server.crt &&
  cp $PWD/client/peers/${PEER}.org-1.svc/tls/keystore/* $PWD/client/peers/${PEER}.org-1.svc/tls/server.key &&
  mkdir -p $PWD/client/peers/${PEER}.org-1.svc/msp/tlscacerts &&
  cp $PWD/client/peers/${PEER}.org-1.svc/tls/tlscacerts/* $PWD/client/peers/${PEER}.org-1.svc/msp/tlscacerts/tlsca.org-1.secureledger.xyz.pem &&
  echo "done"

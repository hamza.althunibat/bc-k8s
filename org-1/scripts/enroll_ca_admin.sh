#!/bin/sh

echo "Enroll the CA admin" &&
    sleep 1 &&
    fabric-ca-client enroll -u https://admin:adminpw@ca.org-1.secureledger.xyz:443 --caname ca-org-1 --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
    echo "done!"

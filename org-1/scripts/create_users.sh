#!/bin/sh

echo "Register user" &&
    fabric-ca-client register --caname ca-org-1 --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
    echo "Register the org admin" &&
    fabric-ca-client register --caname ca-org-1 --id.name amAdmin --id.secret amAdminpw --id.type admin --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
    echo "Generate the user msp" &&
    mkdir -p $PWD/client/users/User1@org-1.svc &&
    fabric-ca-client enroll -u https://user1:user1pw@ca.org-1.secureledger.xyz:443 --caname ca-org-1 -M $PWD/client/users/User1@org-1.svc/msp --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/users/User1@org-1.svc/msp/config.yaml &&
    echo "Generate the org admin msp" &&
    mkdir -p $PWD/client/users/Admin@org-1.svc &&
    fabric-ca-client enroll -u https://amAdmin:amAdminpw@ca.org-1.secureledger.xyz:443 --caname ca-org-1 -M $PWD/client/users/Admin@org-1.svc/msp --tls.certfiles $PWD/rootCA/certs/org-1.secureledger.xyz.crt.pem &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/users/Admin@org-1.svc/msp/config.yaml &&
    echo "done!"

#!/usr/bin/bash

echo "deploying CA root Certificate" &&
    $PWD/scripts/create_root_ca.sh &&
    echo "deploying into K8S ...." &&
    kubectl apply -f deployment/namespace.yaml &&
    kubectl apply -k deployment/ca &&
    read -p "pls ensure dns record is created and workloads are working ...press any key to continue .. " rVar &&
    echo "creating orderer organization artifacts" &&
    $PWD/scripts/create_org.sh

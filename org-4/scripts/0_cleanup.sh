#!/usr/bin/bash
kubectl delete -k deployment/ca
#kubectl delete -k deployment/peers
kubectl delete -f deployment/namespace.yaml
rm -f $PWD/rootCA/certs/*.pem
rm -f $PWD/rootCA/private/*.pem
rm -f $PWD/deployment/ca/ca-root-secret.yaml

rm -f $PWD/client/channel-artifacts/*
rm -f $PWD/client/system-genesis-block/*

rm -rf $PWD/client/msp
rm -rf $PWD/client/users
rm -rf $PWD/client/peers/peer-1.org-4.svc/msp
rm -rf $PWD/client/peers/peer-1.org-4.svc/tls
rm -rf $PWD/client/peers/peer-2.org-4.svc/msp
rm -rf $PWD/client/peers/peer-2.org-4.svc/tls
rm -rf $PWD/client/peers/peer-3.org-4.svc/msp
rm -rf $PWD/client/peers/peer-3.org-4.svc/tls



#!/usr/bin/sh
export PATH="$PATH:$PWD/../bin"
export FABRIC_CA_SERVER_CA_NAME=ca-org-4
export FABRIC_CA_CLIENT_URL=https://ca.org-4.secureledger.xyz
export FABRIC_CA_CLIENT_HOME=$PWD/client
mkdir -p $PWD/client/msp/tlscacerts &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/msp/config.yaml &&
    cp $PWD/rootCA/certs/org-4.secureledger.xyz.crt.pem $PWD/client/msp/tlscacerts/org-4.secureledger.xyz.crt.pem &&
    $PWD/scripts/enroll_ca_admin.sh &&
    $PWD/scripts/create_peer.sh peer-1 &&
    $PWD/scripts/create_peer.sh peer-2 &&
    $PWD/scripts/create_peer.sh peer-3 &&
    $PWD/scripts/create_users.sh

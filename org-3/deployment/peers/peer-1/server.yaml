apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app.kubernetes.io/name: peer-1
    app.kubernetes.io/org: org-3
  name: peer-1
  namespace: org-3
spec:
  replicas: 1
  serviceName: "peer-1"
  selector:
    matchLabels:
      app.kubernetes.io/name: peer-1
      app.kubernetes.io/org: org-3
  template:
    metadata:
      labels:
        app.kubernetes.io/name: peer-1
        app.kubernetes.io/org: org-3
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
              - matchExpressions:
                  - key: kubernetes.io/hostname
                    operator: In
                    values:
                      - k3d-dev-cluster-agent-0
                      - k3d-dev-cluster-agent-1
                      - k3d-dev-cluster-agent-2
      containers:
        - image: hyperledger/fabric-peer:2.3
          name: fabric-peer
          env:
            - name: FABRIC_LOGGING_SPEC
              value: INFO
            - name: CORE_PEER_ADDRESS
              value: peer-1.org-3.svc:7051
            - name: CORE_PEER_GOSSIP_EXTERNALENDPOINT
              value: peer-1.org-3.secureledger.xyz:7051
            - name: CORE_PEER_CHAINCODEADDRESS
              value: peer-1.org-3.svc:7052
            - name: CORE_PEER_CHAINCODELISTENADDRESS
              value: 0.0.0.0:7052
            - name: CORE_PEER_GOSSIP_USELEADERELECTION
              value: "true"
            - name: CORE_PEER_GOSSIP_BOOTSTRAP
              value: peer-1.org-3.svc:7051
            - name: CORE_PEER_ID
              value: peer-1
            - name: CORE_PEER_LOCALMSPID
              value: NZMSP
            - name: CORE_PEER_PROFILE_ENABLED
              value: "true"
            - name: CORE_PEER_TLS_CERT_FILE
              value: /etc/hyperledger/fabric/tls/server.crt
            - name: CORE_PEER_TLS_ENABLED
              value: "true"
            - name: CORE_PEER_TLS_KEY_FILE
              value: /etc/hyperledger/fabric/tls/server.key
            - name: CORE_PEER_TLS_ROOTCERT_FILE
              value: /etc/hyperledger/fabric/tls/ca.crt
            - name: CORE_OPERATIONS_LISTENADDRESS
              value: 0.0.0.0:9443
            - name: CORE_METRICS_PROVIDER
              value: prometheus
            - name: CORE_PEER_TLS_CLIENTAUTHREQUIRED
              value: "false"
            - name: CORE_LEDGER_STATE_STATEDATABASE
              value: CouchDB
            - name: CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS
              value: peer-1-chouchdb.org-3.svc:5984
            - name: CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME
              value: admin
            - name: CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD
              value: adminpw
          args:
            - peer
            - node
            - start
          ports:
            - containerPort: 7051
            - containerPort: 7052
            - containerPort: 7053
            - containerPort: 9443
          resources:
            limits:
              memory: 1Gi
              cpu: 1000m
            requests:
              memory: 256Mi
              cpu: 256m
          volumeMounts:
            - mountPath: /var/hyperledger/production
              name: peer-vol
            - mountPath: /etc/hyperledger/fabric
              name: peer-server-vol
            - mountPath: /opt/hyperledger/builder
              name: builder-vol
            - mountPath: /etc/hyperledger/fabric/core.yaml
              name: core-yaml
              subPath: core.yaml
              readOnly: true
      volumes:
        - name: builder-vol
          hostPath:
            # Ensure the file directory is created.
            path: /org-3/builder
            type: Directory
        - name: peer-server-vol
          hostPath:
            # Ensure the file directory is created.
            path: /org-3/peers/peer-1.org-3.svc
            type: Directory
        - name: core-yaml
          configMap:
            name: fabric-peer-config
            items:
              - key: core.yaml
                path: core.yaml
  volumeClaimTemplates:
    - metadata:
        name: peer-vol
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "local-path"
        resources:
          requests:
            storage: 512Mi
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/name: peer-1
    app.kubernetes.io/org: org-3
  name: peer-1
  namespace: org-3
spec:
  ports:
    - name: https-7051
      port: 7051
      protocol: TCP
      targetPort: 7051
    - name: https-7052
      port: 7052
      protocol: TCP
      targetPort: 7052
    - name: https-7053
      port: 7053
      protocol: TCP
      targetPort: 7053
  selector:
    app.kubernetes.io/name: peer-1
    app.kubernetes.io/org: org-3
  type: LoadBalancer
---
#---------------- peer Metrics Service ---------------
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/name: peer-1
    app.kubernetes.io/org: org-3
    metrics-service: "true"
  name: peer-1-metrics
  namespace: org-3
spec:
  type: ClusterIP
  ports:
    - name: "peer-metrics"
      port: 9443
      targetPort: 9443
  selector:
    app.kubernetes.io/name: peer-1
    app.kubernetes.io/org: org-3

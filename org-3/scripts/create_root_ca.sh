#!/usr/bin/bash
openssl ecparam -name prime256v1 -genkey -noout \
    -out $PWD/rootCA/private/org-3.secureledger.xyz.key.pem &&
    openssl req -config $PWD/rootCA/ca.conf -new -x509 -sha256 -extensions v3_ca \
        -key $PWD/rootCA/private/org-3.secureledger.xyz.key.pem \
        -out $PWD/rootCA/certs/org-3.secureledger.xyz.crt.pem -days 3650 \
        -subj "/C=AE/ST=Dubai/L=/O=Organization Three/OU=IT/CN=org-3.secureledger.xyz" &&
    kubectl create secret tls ca-root --cert=$PWD/rootCA/certs/org-3.secureledger.xyz.crt.pem \
        --key=$PWD/rootCA/private/org-3.secureledger.xyz.key.pem \
        --dry-run=client \
        -n org-3 \
        -o yaml >&$PWD/deployment/ca/ca-root-secret.yaml

#!/usr/bin/bash
#!/usr/bin/sh
export PATH="$PATH:$PWD/../bin"
export FABRIC_CA_SERVER_CA_NAME=ca-orderer
export FABRIC_CA_CLIENT_URL=https://ca.orderer.secureledger.xyz
export FABRIC_CA_CLIENT_HOME=$PWD/client
export FABRIC_CFG_PATH=$PWD/configtx
export FABRIC_LOGGING_SPEC=DEBUG

echo "generating and distributing genesis block" &&
    $PWD/scripts/generate_genesis_block.sh &&
    echo "generating channel and peers anchors" &&
    $PWD/scripts/create_channel_tx_anchors_tx.sh

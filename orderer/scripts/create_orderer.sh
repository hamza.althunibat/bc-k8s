#!/bin/sh

ORDERER=$1
if [ -z "$1" ]; then
  ORDERER=server-0
fi

echo "register peer" &&
  fabric-ca-client register --caname ca-orderer --id.name $ORDERER --id.secret "${ORDERER}pw" --id.type orderer --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
  echo "Generate the peer msp" &&
  fabric-ca-client enroll -u "https://${ORDERER}:${ORDERER}pw@ca.orderer.secureledger.xyz:443" --caname ca-orderer -M $PWD/client/orderers/${ORDERER}.orderer.svc/msp --csr.hosts "${ORDERER}.orderer.svc" --csr.hosts ${ORDERER}.orderer.secureledger.xyz --csr.hosts localhost --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
  echo "copying msp config" &&
  cp $PWD/tmp/msp-config.yaml $PWD/client/orderers/${ORDERER}.orderer.svc/msp/config.yaml &&
  echo "Generate the ${ORDERER} tls certificates" &&
  fabric-ca-client enroll -u "https://${ORDERER}:${ORDERER}pw@ca.orderer.secureledger.xyz:443" --caname ca-orderer -M $PWD/client/orderers/${ORDERER}.orderer.svc/tls --enrollment.profile tls --csr.hosts ${ORDERER}.orderer.svc --csr.hosts ${ORDERER}.orderer.secureledger.xyz --csr.hosts localhost --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
  cp $PWD/client/orderers/${ORDERER}.orderer.svc/tls/tlscacerts/* $PWD/client/orderers/${ORDERER}.orderer.svc/tls/ca.crt &&
  cp $PWD/client/orderers/${ORDERER}.orderer.svc/tls/signcerts/* $PWD/client/orderers/${ORDERER}.orderer.svc/tls/server.crt &&
  cp $PWD/client/orderers/${ORDERER}.orderer.svc/tls/keystore/* $PWD/client/orderers/${ORDERER}.orderer.svc/tls/server.key &&
  mkdir -p $PWD/client/orderers/${ORDERER}.orderer.svc/msp/tlscacerts &&
  cp $PWD/client/orderers/${ORDERER}.orderer.svc/tls/tlscacerts/* $PWD/client/orderers/${ORDERER}.orderer.svc/msp/tlscacerts/tlsca.orderer.secureledger.xyz.pem &&
  echo "done"

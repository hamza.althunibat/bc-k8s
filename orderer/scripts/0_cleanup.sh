#!/usr/bin/bash
kubectl delete -k deployment/ca
kubectl delete -k deployment/orderers
kubectl delete -f deployment/namespace.yaml
rm -f $PWD/rootCA/certs/*.pem
rm -f $PWD/rootCA/private/*.pem
rm -f $PWD/deployment/ca/ca-root-secret.yaml

rm -f $PWD/client/channel-artifacts/*
rm -f $PWD/client/system-genesis-block/*

rm -rf $PWD/client/msp
rm -rf $PWD/client/users
rm -rf $PWD/client/orderers/server-1.orderer.svc/msp
rm -rf $PWD/client/orderers/server-1.orderer.svc/tls
rm -f $PWD/client/orderers/server-1.orderer.svc/genesis.block
rm -rf $PWD/client/orderers/server-2.orderer.svc/msp
rm -rf $PWD/client/orderers/server-2.orderer.svc/tls
rm -f $PWD/client/orderers/server-2.orderer.svc/genesis.block
rm -rf $PWD/client/orderers/server-3.orderer.svc/msp
rm -rf $PWD/client/orderers/server-3.orderer.svc/tls
rm -f $PWD/client/orderers/server-3.orderer.svc/genesis.block

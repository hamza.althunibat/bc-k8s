#!/bin/sh
echo "Enroll the CA admin" &&
    sleep 1 &&
    fabric-ca-client enroll -u https://admin:adminpw@ca.orderer.secureledger.xyz:443 --caname ca-orderer --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
    echo "done!"

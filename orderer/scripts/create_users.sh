#!/bin/sh

echo "Register user" &&
    fabric-ca-client register --caname ca-orderer --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
    echo "Register the org admin" &&
    fabric-ca-client register --caname ca-orderer --id.name ordererAdmin --id.secret ordererAdminpw --id.type admin --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
    echo "Generate the user msp" &&
    mkdir -p $PWD/client/users/User1@orderer.svc &&
    fabric-ca-client enroll -u https://user1:user1pw@ca.orderer.secureledger.xyz:443 --caname ca-orderer -M $PWD/client/users/User1@orderer.svc/msp --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/users/User1@orderer.svc/msp/config.yaml &&
    echo "Generate the org admin msp" &&
    mkdir -p $PWD/client/users/Admin@orderer.svc &&
    fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.secureledger.xyz:443 --caname ca-orderer -M $PWD/client/users/Admin@orderer.svc/msp --tls.certfiles $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/users/Admin@orderer.svc/msp/config.yaml &&
    echo "done!"

#!/bin/sh

configtxgen \
    -profile OrdererGenesis \
    -channelID system-channel \
    -outputBlock $PWD/client/system-genesis-block/genesis.block &&
    cp $PWD/client/system-genesis-block/genesis.block ../org-1/client/system-genesis-block/genesis.block &&
    cp $PWD/client/system-genesis-block/genesis.block ../org-2/client/system-genesis-block/genesis.block &&
    cp $PWD/client/system-genesis-block/genesis.block ../org-3/client/system-genesis-block/genesis.block &&
    cp $PWD/client/system-genesis-block/genesis.block ../org-4/client/system-genesis-block/genesis.block &&
    cp $PWD/client/system-genesis-block/genesis.block ../org-5/client/system-genesis-block/genesis.block

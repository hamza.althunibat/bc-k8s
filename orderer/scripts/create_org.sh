#!/usr/bin/sh
export PATH="$PATH:$PWD/../bin"
export FABRIC_CA_SERVER_CA_NAME=ca-orderer
export FABRIC_CA_CLIENT_URL=https://ca.orderer.secureledger.xyz
export FABRIC_CA_CLIENT_HOME=$PWD/client

mkdir -p $PWD/client/msp/tlscacerts &&
    cp $PWD/tmp/msp-config.yaml $PWD/client/msp/config.yaml &&
    cp $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem $PWD/client/msp/tlscacerts/orderer.secureledger.xyz.crt.pem &&
    $PWD/scripts/enroll_ca_admin.sh &&
    $PWD/scripts/create_orderer.sh server-1 &&
    $PWD/scripts/create_orderer.sh server-2 &&
    $PWD/scripts/create_orderer.sh server-3 &&
    $PWD/scripts/create_users.sh

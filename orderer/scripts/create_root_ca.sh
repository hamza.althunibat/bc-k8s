#!/usr/bin/bash
openssl ecparam -name prime256v1 -genkey -noout \
    -out $PWD/rootCA/private/orderer.secureledger.xyz.key.pem &&
    openssl req -config $PWD/rootCA/ca.conf -new -x509 -sha256 -extensions v3_ca \
        -key $PWD/rootCA/private/orderer.secureledger.xyz.key.pem \
        -out $PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem -days 3650 \
        -subj "/C=AE/ST=Dubai/L=Dubai/O=Orderer Organization/OU=IT/CN=orderer.secureledger.xyz" &&
    kubectl create secret tls ca-root --cert=$PWD/rootCA/certs/orderer.secureledger.xyz.crt.pem \
        --key=$PWD/rootCA/private/orderer.secureledger.xyz.key.pem \
        --dry-run=client \
        -n orderer \
        -o yaml >&$PWD/deployment/ca/ca-root-secret.yaml

#!/bin/sh

echo "Generating for Abc channel"
configtxgen \
	-profile AbcChannel \
	-outputCreateChannelTx \
	$PWD/client/channel-artifacts/abcchannel.tx -channelID abcchannel &&
	for orgmsp in ORG1 ORG3 ORG5; do
		echo "Generating anchor peer update transaction for ${orgmsp}" &&
			configtxgen -profile AbcChannel -outputAnchorPeersUpdate $PWD/client/channel-artifacts/${orgmsp}anchors.tx -channelID abcchannel -asOrg ${orgmsp}
	done &&
	cp $PWD/client/channel-artifacts/abcchannel.tx ../org-1/client/channel-artifacts/abcchannel.tx &&
	cp $PWD/client/channel-artifacts/abcchannel.tx ../org-3/client/channel-artifacts/abcchannel.tx &&
	cp $PWD/client/channel-artifacts/abcchannel.tx ../org-5/client/channel-artifacts/abcchannel.tx &&

	cp $PWD/client/channel-artifacts/ORG1anchors.tx ../org-1/client/channel-artifacts/ORG1anchors.tx &&
	cp $PWD/client/channel-artifacts/ORG3anchors.tx ../org-3/client/channel-artifacts/ORG3anchors.tx &&
	cp $PWD/client/channel-artifacts/ORG5anchors.tx ../org-5/client/channel-artifacts/ORG5anchors.tx

echo "Generating for Xyz channel"
configtxgen \
	-profile XyzChannel \
	-outputCreateChannelTx \
	$PWD/client/channel-artifacts/xyzchannel.tx -channelID xyzchannel &&
	for orgmsp in ORG1 ORG2 ORG4; do
		echo "Generating anchor peer update transaction for ${orgmsp}" &&
			configtxgen -profile XyzChannel -outputAnchorPeersUpdate $PWD/client/channel-artifacts/${orgmsp}anchors.tx -channelID xyzchannel -asOrg ${orgmsp}
	done &&
	cp $PWD/client/channel-artifacts/xyzchannel.tx ../org-1/client/channel-artifacts/xyzchannel.tx &&
	cp $PWD/client/channel-artifacts/xyzchannel.tx ../org-2/client/channel-artifacts/xyzchannel.tx &&
	cp $PWD/client/channel-artifacts/xyzchannel.tx ../org-4/client/channel-artifacts/xyzchannel.tx &&

	cp $PWD/client/channel-artifacts/ORG1anchors.tx ../org-1/client/channel-artifacts/ORG1anchors.tx &&
	cp $PWD/client/channel-artifacts/ORG2anchors.tx ../org-2/client/channel-artifacts/ORG2anchors.tx &&
	cp $PWD/client/channel-artifacts/ORG4anchors.tx ../org-4/client/channel-artifacts/ORG4anchors.tx

apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app.kubernetes.io/name: server-3
    app.kubernetes.io/org: orderer
  name: orderer-3
  namespace: orderer
spec:
  replicas: 1
  serviceName: "orderer-3"
  selector:
    matchLabels:
      app.kubernetes.io/name: server-3
      app.kubernetes.io/org: orderer
  template:
    metadata:
      labels:
        app.kubernetes.io/name: server-3
        app.kubernetes.io/org: orderer
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
              - matchExpressions:
                  - key: kubernetes.io/hostname
                    operator: In
                    values:
                      - k3d-dev-cluster-agent-0
                      - k3d-dev-cluster-agent-1
                      - k3d-dev-cluster-agent-2
      containers:
        - image: hyperledger/fabric-orderer:2.3
          name: fabric-orderer
          env:
            - name: FABRIC_LOGGING_SPEC
              value: INFO
            - name: ORDERER_GENERAL_LISTENADDRESS
              value: "0.0.0.0"
            - name: ORDERER_GENERAL_LISTENPORT
              value: "7050"
            - name: ORDERER_GENERAL_GENESISMETHOD
              value: "file"
            - name: ORDERER_GENERAL_GENESISFILE
              value: /var/hyperledger/orderer/genesis.block
            - name: ORDERER_GENERAL_LOCALMSPID
              value: OrdererMSP
            - name: ORDERER_GENERAL_LOCALMSPDIR
              value: /var/hyperledger/orderer/msp
            - name: ORDERER_GENERAL_TLS_ENABLED
              value: "true"
            - name: ORDERER_GENERAL_TLS_PRIVATEKEY
              value: /var/hyperledger/orderer/tls/server.key
            - name: ORDERER_GENERAL_TLS_CERTIFICATE
              value: /var/hyperledger/orderer/tls/server.crt
            - name: ORDERER_GENERAL_TLS_ROOTCAS
              value: "[/var/hyperledger/orderer/tls/ca.crt]"
            - name: ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE
              value: /var/hyperledger/orderer/tls/server.crt
            - name: ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY
              value: /var/hyperledger/orderer/tls/server.key
            - name: ORDERER_GENERAL_CLUSTER_ROOTCAS
              value: "[/var/hyperledger/orderer/tls/ca.crt]"
            - name: ORDERER_METRICS_PROVIDER
              value: prometheus
            - name: ORDERER_OPERATIONS_LISTENADDRESS
              value: 0.0.0.0:8443
          args:
            - orderer
          resources:
            limits:
              memory: 1Gi
              cpu: 1000m
            requests:
              memory: 256Mi
              cpu: 256m
          volumeMounts:
            - mountPath: /var/hyperledger/production
              name: orderer-vol
            - mountPath: /var/hyperledger/orderer
              name: orderer-server-vol
            - mountPath: /var/hyperledger/orderer/genesis.block
              name: genesis-block-file
              readOnly: true
          ports:
            - containerPort: 7050
            - containerPort: 8443
          workingDir: /opt/gopath/src/github.com/hyperledger/fabric
      volumes:
        - name: genesis-block-file
          hostPath:
            # Ensure the file directory is created.
            path: /orderer/system-genesis-block/genesis.block
            type: File
        - name: orderer-server-vol
          hostPath:
            # Ensure the file directory is created.
            path: /orderer/orderers/server-3.orderer.svc
            type: Directory
  volumeClaimTemplates:
    - metadata:
        name: orderer-vol
      spec:
        accessModes: ["ReadWriteOnce"]
        storageClassName: "local-path"
        resources:
          requests:
            storage: 512Mi
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/name: server-3
    app.kubernetes.io/org: orderer
  name: server-3
  namespace: orderer
spec:
  ports:
    - name: https
      port: 7050
      protocol: TCP
      targetPort: 7050
  selector:
    app.kubernetes.io/name: server-3
    app.kubernetes.io/org: orderer
  type: LoadBalancer
---
#---------------- Orderer1 Metrics Service ---------------
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/name: server-3
    app.kubernetes.io/org: orderer
    metrics-service: "true"
  name: server-3-metrics
  namespace: orderer
spec:
  type: ClusterIP
  ports:
    - name: "orderer-metrics"
      port: 8443
      targetPort: 8443
  selector:
    app.kubernetes.io/name: server-3
    app.kubernetes.io/org: orderer
